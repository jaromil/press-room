# Collection of press for Devuan

## Chimaera release (October 2021)

Devuan 4.0 "Chimaera" has been released, ranked 2nd GNU/Linux distribution worldwide by Distrowatch user ratings, see: https://distrowatch.com/dwres.php?resource=ranking

![](https://upload.wikimedia.org/wikipedia/commons/f/f4/Devuan-logo.svg)

Here an overview in various languages and comment threads:

- EN https://9to5linux.com/devuan-gnulinux-4-0-chimaera-released-for-software-freedom-lovers-based-on-debian-bullseye
- EN https://linuxiac.com/devuan-4-0-chimaera/
- EN https://www.theregister.com/2021/10/15/devuan_4_chimaera_released/
- EN https://www.phoronix.com/scan.php?page=news_item&px=Devuan-4.0-Released
- EN https://news.itsfoss.com/devuan-4-release/
- DE https://www.heise.de/news/Devuan-4-0-Wie-Debian-11-aber-ohne-systemd-6222224.html
- IT https://www.miamammausalinux.org/2021/10/devuan-chimaera-4-0-e-stato-ufficialmente-rilasciato/
- FR https://www.nextinpact.com/lebrief/48473/devuan-4-0-passe-a-debian-11-toujours-sans-systemd
- ES https://www.muylinux.com/2021/10/15/devuan-4-chimaera/
- PT https://sempreupdate.com.br/devuan-gnu-linux-4-0-chimaera-lancado/
- JP https://gihyo.jp/admin/clip/01/linux_dt/202110/15
- CZ https://www.root.cz/zpravicky/devuan-4-0-chimaera-je-debian-11-bullseye-bez-systemd/
- RU https://itbusiness.com.ua/softnews/60975-devuan-linux-predstavlyaet-versiyu-4-0-pod-nazvaniem-chimaera.html
- TR https://www.evrensel.net/yazi/89643/iki-linux-dagitimi

![Init choice at install](https://marketresearchtelecast.com/wp-content/uploads/2021/10/1634677052_Devuan-40-Like-Debian-11-but-without-systemd.png)



-----

```
Devuan is a registered trademark of the Dyne.org foundation.
```



